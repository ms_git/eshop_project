package com.sun.dubbo.webmanage.service.impl;

import javax.annotation.Resource;

import com.sun.dubbo.webmanage.service.TbOrderShippingDubboService;
import com.sun.webmanage.mapper.TbOrderShippingMapper;
import com.sun.webmanage.model.TbOrderShipping;

public class TbOrderShippingDubboServiceImpl implements TbOrderShippingDubboService{
	
	@Resource
	private TbOrderShippingMapper tbOrderShippingMapper;

	@Override
	public boolean insertTbOrderShipping(TbOrderShipping tbOrderShipping) {
		if(tbOrderShippingMapper.insertSelective(tbOrderShipping)>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean updateTbOrderShipping(TbOrderShipping tbOrderShipping) {
		if(tbOrderShippingMapper.updateByPrimaryKeySelective(tbOrderShipping)>0){
			return true;
		}
		return false;
	}

}
