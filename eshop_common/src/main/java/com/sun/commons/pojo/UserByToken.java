package com.sun.commons.pojo;

import java.io.Serializable;

import com.sun.webmanage.model.TbUser;

public class UserByToken implements Serializable{

	private String status;
	private TbUser data;
	private String info;
	
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public TbUser getData() {
		return data;
	}
	public void setData(TbUser data) {
		this.data = data;
	}
	
}
