package com.sun.order.pojo;

import java.io.Serializable;
import java.util.List;

import com.sun.webmanage.model.TbOrder;
import com.sun.webmanage.model.TbOrderItem;
import com.sun.webmanage.model.TbOrderShipping;

public class CreateOrderVO implements Serializable{
	
	private TbOrder tbOrder;//订单信息
	private List<TbOrderItem> orderItems;//订单-商品
	private TbOrderShipping orderShipping;//物流
	private String payment;//实付金额。精确到2位小数;单位:元。如:200.07，表示:200元7分
	private String paymentType;//支付类型，1、在线支付，2、货到付款
	
	public TbOrder getTbOrder() {
		return tbOrder;
	}
	public void setTbOrder(TbOrder tbOrder) {
		this.tbOrder = tbOrder;
	}
	public List<TbOrderItem> getOrderItems() {
		return orderItems;
	}
	public void setOrderItems(List<TbOrderItem> orderItems) {
		this.orderItems = orderItems;
	}
	public TbOrderShipping getOrderShipping() {
		return orderShipping;
	}
	public void setOrderShipping(TbOrderShipping orderShipping) {
		this.orderShipping = orderShipping;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	
}
