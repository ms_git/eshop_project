package com.sun.item.service;

import java.util.Map;

public interface ItemMenuService {

	/**
	 * 加载所有商品类目菜单信息
	 * @return
	 */
	Map<String,Object> listAllCatMenu();
	
}
