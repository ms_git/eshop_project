<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<table class="easyui-datagrid" id="SolrLogList" title="Solr同步记录" 
       data-options="singleSelect:false,collapsible:true,pagination:true,url:'solr/list',method:'get',pageSize:30">
    <thead>
        <tr>
        	<th data-options="field:'id',width:60">序号</th>
            <th data-options="field:'operaType',width:70,formatter:EGO.formatOperaType">操作类型</th>
            <th data-options="field:'operaTime',width:100,formatter:EGO.formatMilliTime">操作用时</th>
            <th data-options="field:'created',width:150,formatter:EGO.formatDateTime">操作时间</th>
            <th data-options="field:'operatorName',width:70">操作人</th>
        </tr>
    </thead>
</table>

