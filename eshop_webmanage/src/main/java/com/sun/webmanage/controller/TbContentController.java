package com.sun.webmanage.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.commons.utils.ActionUtils;
import com.sun.webmanage.model.TbContent;
import com.sun.webmanage.service.TbContentService;

@Controller
public class TbContentController {

	@Resource
	private TbContentService tbContentService;
	
	@ResponseBody
	@RequestMapping("/content/query/list")
	public EasyUIDataGrid listContentPage(int page,int rows,@RequestParam(defaultValue="0") long categoryId){
		return tbContentService.listTbContentPage(page, rows, categoryId);
	}
	
	@ResponseBody
	@RequestMapping("/content/save")
	public Map<String,Object> saveTbContent(TbContent tbContent){
		if(tbContentService.saveTbContent(tbContent)){
			return ActionUtils.ajaxSuccess("新增商城内容成功", null);
		}
		return ActionUtils.ajaxFail("新增商城内容失败", null);
	}
	
	@ResponseBody
	@RequestMapping("/rest/content/edit")
	public Map<String,Object> editTbContent(TbContent tbContent){
		if(tbContentService.editTbContent(tbContent)){
			return ActionUtils.ajaxSuccess("更新商城内容成功", null);
		}
		return ActionUtils.ajaxFail("更新商城内容失败", null);
	}
	
	@ResponseBody
	@RequestMapping("content/delete")
	public Map<String,Object> delTbContent(String ids){
		if(tbContentService.delTbContent(ids)){
			return ActionUtils.ajaxSuccess("内容删除成功", "");
		}else{
			return ActionUtils.ajaxFail("内容删除失败", "");
		}
	}
	
}
