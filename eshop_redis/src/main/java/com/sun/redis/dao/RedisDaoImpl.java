package com.sun.redis.dao;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import redis.clients.jedis.JedisCluster;

@Repository
public class RedisDaoImpl implements RedisDao{
	
	@Resource
	private JedisCluster jedisClients;
	

	@Override
	public String getKey(String key) {
		return jedisClients.get(key);
	}

	@Override
	public String setKey(String key, String value) {
		return jedisClients.set(key, value);
	}

	@Override
	public boolean exists(String key) {
		return jedisClients.exists(key);
	}

	@Override
	public long expire(String key,int time_in_seconds) {
		return jedisClients.expire(key, time_in_seconds);
	}

	@Override
	public long del(String key_name) {
		return jedisClients.del(key_name);
	}

	@Override
	public long del(String... keys) {
		return jedisClients.del(keys);
	}

}
